#pragma once
#include "ShaderBuffer.h"
#include "DirectXFramework.h"

struct Bone {
	int id;
	Bone* firstChild;
	Bone* sibling;

	XMMATRIX offsetMat;
	XMMATRIX initMat;
	XMMATRIX boneMat;
	XMMATRIX* combMatAry;

	Bone() {
		ZeroMemory(this, sizeof(Bone));
		offsetMat = initMat = boneMat = XMMatrixIdentity();
	}
};

struct Material {
	XMFLOAT4 color;
	XMFLOAT4 emissive;
	XMFLOAT3 specular;
	float specular_power;
	ID3D11ShaderResourceView* pTexture = NULL;
	DWORD faceNum;

	Material() {
		ZeroMemory(this, sizeof(Material));
	}

	~Material() {
		SAFE_RELEASE(pTexture);
	}

};

class Drawable {
public:
	Drawable() {};
	virtual ~Drawable();
	void SetShader(TCHAR* VertexShaderFile, CHAR* VertexShaderMain, TCHAR* PixelShaderFile, CHAR* PixelShaderMain, const D3D11_INPUT_ELEMENT_DESC* layout, const UINT numElements);
protected:
	ID3D11Device* pDevice = nullptr;
	ID3D11DeviceContext* pImmediateContext = nullptr;
	ID3D11VertexShader*	pVertexShader = nullptr;
	ID3D11InputLayout*	pVertexLayout = nullptr;
	ID3D11PixelShader*	pPixelShader = nullptr;

	ID3D11Buffer* pVertexBuffer = nullptr;
	ID3D11Buffer** ppIndexBuffer = nullptr;

	DWORD indexNum;
	DWORD vertexNum;
	DWORD faceNum;
};

class HandMesh : Drawable{
public:
	HandMesh(ID3D11Device* pDev, ID3D11DeviceContext* pImm);
	~HandMesh();
	void Draw(const XMFLOAT3& pos = XMFLOAT3(0.0f, 0.0f, 0.0f), const XMFLOAT3 &rot = XMFLOAT3(0.0f, 0.0f, 0.0f), const XMFLOAT3 &scale = XMFLOAT3(1.0f, 1.0f, 1.0f));
protected:
	ID3D11Buffer* pConstantMeshBuffer		= nullptr;
	ID3D11Buffer* pConstantMaterialBuffer	= nullptr;
	ID3D11Buffer* pConstantBoneBuffer		= nullptr;
	ID3D11SamplerState* pSampleLinear		= nullptr;

	static Bone pBone[256];
	Material* pMaterial = nullptr;

	void CalcRelativeMat(Bone* me, const XMMATRIX &parentOffsetMat);
	void UpdateBone(Bone* me, const XMMATRIX &parentWorldMat);
private:
};

