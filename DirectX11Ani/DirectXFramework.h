#pragma once

class DirectXFramework {
public:
	DirectXFramework(HWND h);
	~DirectXFramework();
	void BeginScene();
	void EndScene();
	void Run();

	ID3D11Device* GetDevice();
	ID3D11DeviceContext* GetImmediateConstext();
private:
	HWND hWnd = NULL;

	ID3D11Device*			pDevice = NULL;
	ID3D11DeviceContext* pImmediateContext = NULL;
	IDXGISwapChain*			pSwapChain = NULL;
	ID3D11RenderTargetView* pRenderTargetView = NULL;
	ID3D11DepthStencilView* pDepthStencilView = NULL;
	ID3D11Texture2D*		pDepthStencil = NULL;
};