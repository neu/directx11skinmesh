#pragma once
#include "ShaderBuffer.h"
#include "DirectXFramework.h"

class Camera {
public:
	Camera(ID3D11Device* pDev, ID3D11DeviceContext* pImm);
	~Camera();
	void SetCamera(const XMFLOAT3 &eye_t, const XMFLOAT3 &focus_t);
	void SetCamera();
	void SetUpDirection(const XMFLOAT3 &u);
	void SetViewport(const float w, const float h);
	void SetAngle(const float a);
	void SetDepth(const float min_d, const float max_d);
private:
	ID3D11Device* pDevice = nullptr;
	ID3D11DeviceContext* pImmediateContext = nullptr;
	ID3D11Buffer* pConstantBuffer = nullptr;

	XMFLOAT3 eye;
	XMFLOAT3 focus;
	XMFLOAT3 upDirection;
	float view_width;
	float view_height;
	float angle;
	float min_depth;
	float max_depth;
};

