
#include "stdafx.h"
#include "Mesh.h"

Bone HandMesh::pBone[256];


Drawable::~Drawable(){
	SAFE_RELEASE(pVertexBuffer);
	for (int i = 0; i < indexNum; i++) {
		SAFE_RELEASE(ppIndexBuffer[i]);
	}
	SAFE_DELETE_ARRAY(ppIndexBuffer);
	SAFE_RELEASE(pVertexShader);
	SAFE_RELEASE(pPixelShader);
	SAFE_RELEASE(pDevice);
}

void Drawable::SetShader(TCHAR * VertexShaderFile, CHAR * VertexShaderMain, TCHAR * PixelShaderFile, CHAR * PixelShaderMain, const D3D11_INPUT_ELEMENT_DESC* layout, const UINT numElements)
{

	SAFE_RELEASE(pVertexShader);
	SAFE_RELEASE(pPixelShader);

	//hlslファイル読み込み ブロブ作成　ブロブとはシェーダーの塊みたいなもの。XXシェーダーとして特徴を持たない。後で各種シェーダーに成り得る。
	ID3DBlob *pCompiledShader = NULL;
	ID3DBlob *pErrors = NULL;
	//ブロブからバーテックスシェーダー作成
	if (FAILED(D3DX11CompileFromFile(VertexShaderFile, NULL, NULL, VertexShaderMain, "vs_5_0", 0, 0, NULL, &pCompiledShader, &pErrors, NULL)))
	{
		MessageBox(0, L"hlsl読み込み失敗", NULL, MB_OK);
	}
	SAFE_RELEASE(pErrors);

	if (FAILED(pDevice->CreateVertexShader(pCompiledShader->GetBufferPointer(), pCompiledShader->GetBufferSize(), NULL, &pVertexShader)))
	{
		SAFE_RELEASE(pCompiledShader);
		MessageBox(0, L"バーテックスシェーダー作成失敗", NULL, MB_OK);
	}

	//頂点インプットレイアウトを作成
	if (FAILED(pDevice->CreateInputLayout(layout, numElements, pCompiledShader->GetBufferPointer(), pCompiledShader->GetBufferSize(), &pVertexLayout))) {
		MessageBox(0, L"pDevice->CreateInputLayout", NULL, MB_OK);
	}
	//頂点インプットレイアウトをセット
	pImmediateContext->IASetInputLayout(pVertexLayout);

	//ブロブからピクセルシェーダー作成
	if (FAILED(D3DX11CompileFromFile(PixelShaderFile, NULL, NULL, PixelShaderMain, "ps_5_0", 0, 0, NULL, &pCompiledShader, &pErrors, NULL)))
	{
		MessageBox(0, L"hlsl読み込み失敗", NULL, MB_OK);
	}
	SAFE_RELEASE(pErrors);
	if (FAILED(pDevice->CreatePixelShader(pCompiledShader->GetBufferPointer(), pCompiledShader->GetBufferSize(), NULL, &pPixelShader)))
	{
		SAFE_RELEASE(pCompiledShader);
		MessageBox(0, L"ピクセルシェーダー作成失敗", NULL, MB_OK);
	}
	SAFE_RELEASE(pCompiledShader);
}

HandMesh::HandMesh(ID3D11Device * pDev, ID3D11DeviceContext * pImm)
{
	pDevice = pDev;
	pImmediateContext = pImm;

	//頂点インプットレイアウトを定義	
	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION",     0, DXGI_FORMAT_R32G32B32_FLOAT,    0, 0,  D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL"  ,     0, DXGI_FORMAT_R32G32B32_FLOAT,    0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD",     0, DXGI_FORMAT_R32G32_FLOAT,       0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "BONEINDICES",  0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 32, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "BONEWEIGHT",   0, DXGI_FORMAT_R32G32B32_FLOAT,    0, 48, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	UINT numElements = sizeof(layout) / sizeof(layout[0]);

	SetShader(_T("AniShader.hlsl"), "VS", _T("AniShader.hlsl"), "PS", layout, numElements);

	// 頂点データ
	AniVertex vertices[] = {
		{ XMFLOAT3( 4.0f,  0.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, -1.0f), XMFLOAT2(1.0f, 1.00f), { 0, 0, 0, 0 }, XMFLOAT3(1.0f, 0.0f, 0.00f) },
		{ XMFLOAT3(-4.0f,  0.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, -1.0f), XMFLOAT2(0.0f, 1.00f), { 0, 0, 0, 0 }, XMFLOAT3(1.0f, 0.0f, 0.00f) },
		{ XMFLOAT3( 4.0f,  2.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, -1.0f), XMFLOAT2(1.0f, 0.75f), { 0, 1, 0, 0 }, XMFLOAT3(0.5f, 0.5f, 0.00f) },
		{ XMFLOAT3(-4.0f,  2.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, -1.0f), XMFLOAT2(0.0f, 0.75f), { 0, 1, 0, 0 }, XMFLOAT3(0.5f, 0.5f, 0.00f) },
		{ XMFLOAT3( 4.0f,  4.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, -1.0f), XMFLOAT2(1.0f, 0.50f), { 1, 0, 0, 0 }, XMFLOAT3(1.0f, 0.0f, 0.00f) },
		{ XMFLOAT3(-4.0f,  4.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, -1.0f), XMFLOAT2(0.0f, 0.50f), { 1, 0, 0, 0 }, XMFLOAT3(1.0f, 0.0f, 0.00f) },
		{ XMFLOAT3( 4.0f,  6.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, -1.0f), XMFLOAT2(1.0f, 0.25f), { 1, 2, 0, 0 }, XMFLOAT3(0.5f, 0.5f, 0.00f) },
		{ XMFLOAT3(-4.0f,  6.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, -1.0f), XMFLOAT2(0.0f, 0.25f), { 1, 2, 0, 0 }, XMFLOAT3(0.5f, 0.5f, 0.00f) },
		{ XMFLOAT3( 4.0f,  8.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, -1.0f), XMFLOAT2(1.0f, 0.00f), { 2, 0, 0, 0 }, XMFLOAT3(1.0f, 0.0f, 0.00f) },
		{ XMFLOAT3(-4.0f,  8.0f, 0.0f) ,XMFLOAT3(0.0f, 0.0f, -1.0f), XMFLOAT2(0.0f, 0.00f), { 2, 0, 0, 0 }, XMFLOAT3(1.0f, 0.0f, 0.00f) },
	};

	vertexNum = sizeof(vertices)/sizeof(AniVertex);

	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(AniVertex) * vertexNum;
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;
	D3D11_SUBRESOURCE_DATA InitData;
	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = vertices;
	if (FAILED(pDevice->CreateBuffer(&bd, &InitData, &pVertexBuffer))) {
		MessageBox(NULL, _T("HandMesh::HandMesh"), _T("errer"), MB_OK);
	}

	// インデックスを作成
	indexNum = 1;
	SimpleIndex indeces[] = {
		1, 2, 0,
		1, 3, 2,
		3, 4, 2,
		3, 5, 4,
		5, 6, 4,
		5, 7, 6,
		7, 8, 6,
		7, 9, 8,
	};

	faceNum = sizeof(indeces)/sizeof(SimpleIndex)/3;

	ppIndexBuffer = new ID3D11Buffer*[indexNum];
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(SimpleIndex) * faceNum * 3;
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;
	bd.MiscFlags = 0;

	InitData.pSysMem = indeces;
	InitData.SysMemPitch = 0;
	InitData.SysMemSlicePitch = 0;
	if (FAILED(pDevice->CreateBuffer(&bd, &InitData, &ppIndexBuffer[0]))) {
		MessageBox(NULL, _T("HandMesh::HandMesh"), _T("errer"), MB_OK);
	}

	// 定数バッファ
	// メッシュ用
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(ShaderBufferMesh);
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;
	if (FAILED(pDevice->CreateBuffer(&bd, nullptr, &pConstantMeshBuffer))) {
		MessageBox(NULL, _T("HandMesh::HandMesh"), _T("errer"), MB_OK);
	}

	// マテリアル用
	bd.ByteWidth = sizeof(ShaderBufferMaterial);
	if (FAILED(pDevice->CreateBuffer(&bd, nullptr, &pConstantMaterialBuffer))) {
		MessageBox(NULL, _T("HandMesh::HandMesh"), _T("errer"), MB_OK);
	}

	// ボーン用
	bd.ByteWidth = sizeof(ShaderBufferBones);
	if (FAILED(pDevice->CreateBuffer(&bd, nullptr, &pConstantBoneBuffer))) {
		MessageBox(NULL, _T("HandMesh::HandMesh"), _T("errer"), MB_OK);
	}

	// マテリアル
	pMaterial = new Material[1];
	pMaterial[0].color    = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	pMaterial[0].emissive = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	pMaterial[0].specular = XMFLOAT3(1.0f, 1.0f, 1.0f);
	pMaterial[0].specular_power = 0.10f;
	if (FAILED(D3DX11CreateShaderResourceViewFromFile(pDevice, _T("test.jpg"), NULL, NULL, &pMaterial[0].pTexture, NULL))) {
		MessageBox(NULL, _T("D3DX11CreateShaderResourceViewFromFile"), _T("errer"), MB_OK);
	}
	pMaterial[0].faceNum = faceNum;

	// テクスチャサンプラー
	D3D11_SAMPLER_DESC sd;
	ZeroMemory(&sd, sizeof(D3D11_SAMPLER_DESC));

	sd.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	sd.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	sd.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	sd.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	pDevice->CreateSamplerState(&sd, &pSampleLinear);

	// ボーン
	// 親子関係
	pBone[0].firstChild = &pBone[1];
	pBone[1].firstChild = &pBone[2];

	// 初期姿勢の計算
	// まずはローカル姿勢を設定して
	// 最終的に自分の親からの相対姿勢に直します。
	pBone[0].initMat = XMMatrixRotationY(XMConvertToRadians(0.0f));
	pBone[1].initMat = XMMatrixRotationY(XMConvertToRadians(0.0f));
	pBone[2].initMat = XMMatrixRotationY(XMConvertToRadians(0.0f));

	pBone[0].initMat *= XMMatrixTranslation(0.0f, 0.0f, 0.0f);
	pBone[1].initMat *= XMMatrixTranslation(0.0f, 4.0f, 0.0f);
	pBone[2].initMat *= XMMatrixTranslation(0.0f, 8.0f, 0.0f);

	for (int i = 0; i < sizeof(pBone)/sizeof(Bone); i++) {
		pBone[i].offsetMat = XMMatrixInverse(nullptr, pBone[i].initMat);
	}

	CalcRelativeMat(&pBone[0], XMMatrixIdentity());
	
}

void HandMesh::CalcRelativeMat(Bone* me, const XMMATRIX &parentOffsetMat) {
	if (me->firstChild) {
		CalcRelativeMat(me->firstChild, me->offsetMat);
	}
	if (me->sibling) {
		CalcRelativeMat(me->sibling, parentOffsetMat);
	}
	
	me->initMat *= parentOffsetMat;
}

HandMesh::~HandMesh()
{
	SAFE_DELETE_ARRAY(pMaterial);
	SAFE_RELEASE(pSampleLinear);
	SAFE_RELEASE(pConstantMeshBuffer);
	SAFE_RELEASE(pConstantMaterialBuffer);
	SAFE_RELEASE(pConstantBoneBuffer);
}

void HandMesh::UpdateBone(Bone* me, const XMMATRIX &parentWorldMat) {
	me->boneMat *= parentWorldMat;
	if (me->firstChild) {
		UpdateBone(me->firstChild, me->boneMat);
	}

	if (me->sibling) {
		UpdateBone(me->sibling, parentWorldMat);
	}

	me->boneMat = me->offsetMat * me->boneMat;
}

void HandMesh::Draw(const XMFLOAT3& pos, const XMFLOAT3 &rot, const XMFLOAT3 &scale)
{
	// シェーダをセット
	pImmediateContext->VSSetShader(pVertexShader, nullptr, 0);
	pImmediateContext->PSSetShader(pPixelShader, nullptr, 0);

	// 頂点データをセット
	UINT stride = sizeof(AniVertex);
	UINT offset = 0;
	pImmediateContext->IASetVertexBuffers(0, 1, &pVertexBuffer, &stride, &offset);

	// インデックスをセット
	pImmediateContext->IASetIndexBuffer(ppIndexBuffer[0], DXGI_FORMAT_R32_UINT, 0);

	// 定数をセット
	// メッシュ用
	ShaderBufferMesh mb;
	XMMATRIX world = XMMatrixIdentity();
	world = world * XMMatrixScaling(scale.x, scale.y, scale.z);
	world = world * XMMatrixRotationX(XMConvertToRadians(rot.x)) * XMMatrixRotationY(XMConvertToRadians(rot.y)) * XMMatrixRotationZ(XMConvertToRadians(rot.z));
	world = world * XMMatrixTranslation(pos.x, pos.y, pos.z);
	mb.mWorld = XMMatrixTranspose(world);
	pImmediateContext->UpdateSubresource(pConstantMeshBuffer, 0, nullptr, &mb, 0, 0);
	pImmediateContext->VSSetConstantBuffers(SHADER_REGISTER_MESH, 1, &pConstantMeshBuffer);

	// マテリアル
	ShaderBufferMaterial bm;
	bm.color    = pMaterial[0].color;
	bm.emissive = pMaterial[0].emissive;
	bm.specular = pMaterial[0].specular;
	bm.specular_power = pMaterial[0].specular_power;
	pImmediateContext->UpdateSubresource(pConstantMaterialBuffer, 0, nullptr, &bm, 0, 0);
	pImmediateContext->PSSetConstantBuffers(SHADER_REGISTER_MATERIAL, 1, &pConstantMaterialBuffer);

	// ボーン用
	ShaderBufferBones bb;
	static float val;
	val += 0.0003;

	XMMATRIX defBone[7];
	defBone[0] = XMMatrixRotationZ(XMConvertToRadians(val*0));
	for (int i = 1; i < 7; i++) {
		defBone[i] = XMMatrixRotationZ( XMConvertToRadians(sinf(val) * 30));
	}

	for (int i = 0; i < 7; i++) {
		pBone[i].boneMat = defBone[i] * pBone[i].initMat;
		i = i;
	}

	// 各自ベースで姿勢を更新
	UpdateBone(&pBone[0], XMMatrixRotationX(XMConvertToRadians(180.0f)));
	XMMATRIX mat;
	for (int i = 0; i < 7; i++) {
		bb.mat[i] = pBone[i].boneMat;
		i = i;
	}

	pImmediateContext->UpdateSubresource(pConstantBoneBuffer, 0, nullptr, &bb, 0, 0);
	pImmediateContext->VSSetConstantBuffers(SHADER_REGISTER_BONE, 1, &pConstantBoneBuffer);

	// テクスチャを渡す
	if (pMaterial[0].pTexture != NULL) {
		pImmediateContext->PSSetSamplers(0, 1, &pSampleLinear);
		pImmediateContext->PSSetShaderResources(0, 1, &pMaterial[0].pTexture);
	}

	// プリミティブをセット
	pImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	pImmediateContext->DrawIndexed(pMaterial[0].faceNum*3, 0, 0);
}


