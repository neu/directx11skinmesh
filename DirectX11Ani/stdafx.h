// stdafx.h : 標準のシステム インクルード ファイルのインクルード ファイル、または
// 参照回数が多く、かつあまり変更されない、プロジェクト専用のインクルード ファイル
// を記述します。
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Windows ヘッダーから使用されていない部分を除外します。
// Windows ヘッダー ファイル:
#include <windows.h>

// C ランタイム ヘッダー ファイル
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>


// 必要なライブラリをリンクする
#pragma comment( lib, "d3d11.lib" )
#if defined(DEBUG) || defined(_DEBUG)
#pragma comment( lib, "d3dx11d.lib" )
#else
#pragma comment( lib, "d3dx11.lib" )
#endif
#pragma comment( lib, "dxerr.lib" )
#pragma comment( lib, "dxgi.lib" )

// TODO: プログラムに必要な追加ヘッダーをここで参照してください。
#define DIRECTINPUT_VERSION 0x0800
#define SAFE_RELEASE(p) { if(p) { (p)->Release(); (p)=NULL; } }
#define SAFE_DELETE(p) { if(p) { delete (p); (p)=NULL; } }
#define SAFE_DELETE_ARRAY(p) { if(p) { delete[] (p);   (p)=NULL; } }
#include <iostream>
#include <D3DX11.h>
#include <dinput.h>
#include <mmsystem.h>
#include <Dsound.h>
#include <fstream>
#include <vector>
#include <list>
#include <crtdbg.h>
#include <DirectXMath.h>
#include <dxerr.h>
using namespace std;
using namespace DirectX;

#pragma comment(lib,"winmm.lib")
const int WINDOW_WIDTH = 640;
const int WINDOW_HEIGHT = 480;
const float CAMERA_VIEW_ANGLE = XM_PIDIV2;
const float CAMERA_MIN_DEPTH = 1.0f;
const float CAMERA_MAX_DEPTH = 10000.0f;

// ここ
//必要なヘッダーファイルのインクルード
#include <d3dCompiler.h>
//必要なライブラリファイルのロード
#pragma comment(lib,"d3dx10.lib")
#pragma comment(lib,"d3dCompiler.lib")
