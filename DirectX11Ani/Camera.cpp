
#include "stdafx.h"
#include "Camera.h"

Camera::Camera(ID3D11Device * pDev, ID3D11DeviceContext * pImm)
{
	pDevice = pDev;
	pImmediateContext = pImm;

	// 定数バッファを作成
	D3D11_BUFFER_DESC cb;
	ZeroMemory(&cb, sizeof(cb));
	cb.Usage = D3D11_USAGE_DEFAULT;
	cb.ByteWidth = sizeof(ShaderBufferCamera);
	cb.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cb.CPUAccessFlags = 0;
	if (FAILED(pDevice->CreateBuffer(&cb, nullptr, &pConstantBuffer))) {
		MessageBox(NULL, _T("Camera::Camera pDevice->CreateBuffer"), _T("errer"), MB_OK);
	}

	SetUpDirection(XMFLOAT3(0.0f, 1.0f, 0.0f));
	SetViewport(WINDOW_WIDTH, WINDOW_HEIGHT);
	SetAngle(CAMERA_VIEW_ANGLE);
	SetDepth(CAMERA_MIN_DEPTH, CAMERA_MAX_DEPTH);
	SetCamera(XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, 1.0f));
}

Camera::~Camera()
{
	SAFE_RELEASE(pConstantBuffer);
}

void Camera::SetCamera(const XMFLOAT3 & eye_t, const XMFLOAT3 &focus_t)
{
	eye = eye_t;
	focus = focus_t;

	if (eye.x == focus.x && eye.y == focus.y && eye.z == focus.z) {
		return;
	}

	ShaderBufferCamera camera;
	camera.mView = XMMatrixLookAtLH(XMLoadFloat3(&eye), XMLoadFloat3(&focus), XMLoadFloat3(&upDirection));
	camera.mView = XMMatrixTranspose(camera.mView);

	camera.mProj = XMMatrixPerspectiveFovLH(angle, view_width / view_height, min_depth, max_depth);
	camera.mProj = XMMatrixTranspose(camera.mProj);

	camera.eye = eye;

	pImmediateContext->UpdateSubresource(pConstantBuffer, 0, nullptr, &camera, 0, 0);
	pImmediateContext->VSSetConstantBuffers(SHADER_REGISTER_CAMERA, 1, &pConstantBuffer);
	pImmediateContext->PSSetConstantBuffers(SHADER_REGISTER_CAMERA, 1, &pConstantBuffer);
}

void Camera::SetCamera()
{
	this->SetCamera(eye, focus);
}

void Camera::SetUpDirection(const XMFLOAT3 & u)
{
	upDirection = u;
}

void Camera::SetViewport(const float w, const float h)
{
	view_width  = w;
	view_height = h;
}

void Camera::SetAngle(const float a)
{
	angle = a;
}

void Camera::SetDepth(const float min_d, const float max_d)
{
	min_depth = min_d;
	max_depth = max_d;
}
