
#include "stdafx.h"
#include "MyApplication.h"

MyApplication::MyApplication(HWND hWnd) {
	directXFramework = new DirectXFramework(hWnd);
	pDevice = directXFramework->GetDevice();
	pImmediateContext = directXFramework->GetImmediateConstext();

	handMesh = new HandMesh(pDevice, pImmediateContext);
	pCamera = new Camera(pDevice, pImmediateContext);
}

MyApplication::~MyApplication()
{
	SAFE_DELETE(directXFramework);
	SAFE_DELETE(handMesh);
	SAFE_DELETE(pCamera)
	SAFE_RELEASE(pDevice);
	SAFE_RELEASE(pImmediateContext);
}

void MyApplication::Run()
{
	directXFramework->BeginScene();

	static float rot = 0.0f;
	rot += 0.02f;
	pCamera->SetAngle(30);
	pCamera->SetCamera(XMFLOAT3(0.0f, -5.0f, -14.0f), XMFLOAT3(0.0f, 0.0f, 0.0f));
	handMesh->Draw(XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(1.0f, 1.0f, 1.0f));

	directXFramework->EndScene();
}
