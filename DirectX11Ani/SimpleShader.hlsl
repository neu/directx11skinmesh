

cbuffer MeshBuffer:register(b0) {
	matrix world;	// ワールド変換行列
}

cbuffer CameraBuffer : register(b1) {
	matrix view;	// ビュー変換行列
	matrix proj;	// 射影変換行列
}

struct VS_INPUT {
	float4 Pos		: POSITION;
	float3 Normal	: NORMAL;
};

struct PS_INPUT {
	float4 Pos		: SV_POSITION;
	float3 Normal	: TEXCOORD0;
};

PS_INPUT VS(float4 Pos : POSITION, float4 Normal : NORMAL) 
{
	PS_INPUT output;
	output.Pos = mul(Pos, world);
	output.Pos = mul(output.Pos, view);
	output.Pos = mul(output.Pos, proj);
	output.Normal = mul(Normal, world);
	return output;
}

float4 PS(PS_INPUT input) : SV_Target
{
	float4 color = float4(1.0f, 0.0f, 1.0f, 1.0f);
	float3 light = float3(0.0f, 0.0f, 1.0f);
	light = normalize(light);
	float3 normal = normalize(input.Normal);
	float4 NL = saturate(2 * dot(normal, light));
	return color * NL;
}

