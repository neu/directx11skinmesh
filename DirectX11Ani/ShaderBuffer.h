#pragma once
#include "stdafx.h"

const int BONE_MAX = 256;

struct SimpleVertex {
	XMFLOAT3 pos;
	XMFLOAT3 normal;
};

struct AniVertex {
	XMFLOAT3 pos;
	XMFLOAT3 normal;
	XMFLOAT2 uv;
	UINT boneNum[4];
	XMFLOAT3 boneWeight;
};

struct SimpleIndex {
	unsigned int index;
};

enum {
	SHADER_REGISTER_MESH = 0,
	SHADER_REGISTER_CAMERA,
	SHADER_REGISTER_MATERIAL,
	SHADER_REGISTER_BONE,
};

struct ShaderBufferMesh {
	XMMATRIX mWorld;
};

struct ShaderBufferCamera {
	XMMATRIX mView;
	XMMATRIX mProj;
	XMFLOAT3 eye;
	float dummy;
};

struct ShaderBufferMaterial {
	XMFLOAT4 color;
	XMFLOAT4 emissive;
	XMFLOAT3 specular;
	float specular_power;
};

struct ShaderBufferBones {
	ShaderBufferBones() {
		for (auto &m : mat) {
			m = XMMatrixIdentity();
		}
	}
	XMMATRIX mat[256];
};
