#pragma once
#include "DirectXFramework.h"
#include "Mesh.h"
#include "Camera.h"

class MyApplication {
public:
	MyApplication(HWND hWnd);
	~MyApplication();
	void Run();
private:
	ID3D11Device* pDevice;
	ID3D11DeviceContext* pImmediateContext = NULL;
	DirectXFramework* directXFramework;

	HandMesh* handMesh;
	Camera* pCamera;
};
