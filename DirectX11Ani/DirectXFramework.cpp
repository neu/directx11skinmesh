
#include "stdafx.h"
#include "DirectXFramework.h"

DirectXFramework::DirectXFramework(HWND h) : hWnd(h) {
	// デバイスとスワップチェーンの作成
	DXGI_SWAP_CHAIN_DESC sd;
	ZeroMemory(&sd, sizeof(sd));
	sd.BufferCount = 1;
	sd.BufferDesc.Width = WINDOW_WIDTH;
	sd.BufferDesc.Height = WINDOW_HEIGHT;
	sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	sd.BufferDesc.RefreshRate.Numerator = 60;
	sd.BufferDesc.RefreshRate.Denominator = 1;
	sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	sd.OutputWindow = hWnd;
	sd.SampleDesc.Count = 1;
	sd.SampleDesc.Quality = 0;
	sd.Windowed = TRUE;

	D3D_FEATURE_LEVEL pFeatureLevels = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL* pFeatureLevel = NULL;

	if (FAILED(D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL,
		0, &pFeatureLevels, 1, D3D11_SDK_VERSION, &sd, &pSwapChain, &pDevice,
		pFeatureLevel, &pImmediateContext)))
	{
		MessageBox(0, L"D3D11CreateDeviceAndSwapChain", NULL, MB_OK);
	}
	//レンダーターゲットビューの作成
	ID3D11Texture2D *pBackBuffer;
	pSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer);
	pDevice->CreateRenderTargetView(pBackBuffer, NULL, &pRenderTargetView);
	SAFE_RELEASE(pBackBuffer);

	//深度ステンシルビューの作成
	D3D11_TEXTURE2D_DESC descDepth;
	descDepth.Width = WINDOW_WIDTH;
	descDepth.Height = WINDOW_HEIGHT;
	descDepth.MipLevels = 1;
	descDepth.ArraySize = 1;
	descDepth.Format = DXGI_FORMAT_D32_FLOAT;
	descDepth.SampleDesc.Count = 1;
	descDepth.SampleDesc.Quality = 0;
	descDepth.Usage = D3D11_USAGE_DEFAULT;
	descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	descDepth.CPUAccessFlags = 0;
	descDepth.MiscFlags = 0;
	pDevice->CreateTexture2D(&descDepth, NULL, &pDepthStencil);

	pDevice->CreateDepthStencilView(pDepthStencil, NULL, &pDepthStencilView);
	//レンダーターゲットビューと深度ステンシルビューをパイプラインにバインド	
	pImmediateContext->OMSetRenderTargets(1, &pRenderTargetView, pDepthStencilView);
	//ビューポートの設定
	D3D11_VIEWPORT vp;
	vp.Width = WINDOW_WIDTH;
	vp.Height = WINDOW_HEIGHT;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	pImmediateContext->RSSetViewports(1, &vp);


	//ラスタライズ設定
	D3D11_RASTERIZER_DESC rdc;
	ZeroMemory(&rdc, sizeof(rdc));
	rdc.CullMode = D3D11_CULL_NONE;
	rdc.FillMode = D3D11_FILL_SOLID;

	ID3D11RasterizerState* pIr = NULL;
	pDevice->CreateRasterizerState(&rdc, &pIr);
	pImmediateContext->RSSetState(pIr);
	SAFE_RELEASE(pIr);

}

DirectXFramework::~DirectXFramework() {
	SAFE_RELEASE(pDepthStencilView);
	SAFE_RELEASE(pDepthStencil);
	SAFE_RELEASE(pSwapChain);
	SAFE_RELEASE(pRenderTargetView);
	SAFE_RELEASE(pImmediateContext);
	SAFE_RELEASE(pDevice);

}

void DirectXFramework::BeginScene()
{
	float ClearColor[4] = { 0,0,1,1 }; // クリア色作成　RGBAの順
	pImmediateContext->ClearRenderTargetView(pRenderTargetView, ClearColor);//画面クリア 
	pImmediateContext->ClearDepthStencilView(pDepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);//深度バッファクリア
}

void DirectXFramework::EndScene()
{
	pSwapChain->Present(0, 0);//画面更新（バックバッファをフロントバッファに）	
}

ID3D11Device * DirectXFramework::GetDevice()
{
	return pDevice;
}

ID3D11DeviceContext * DirectXFramework::GetImmediateConstext()
{
	return pImmediateContext;
}

void DirectXFramework::Run() {

}
