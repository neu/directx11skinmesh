
// グローバル変数
Texture2D g_texDecal:register(t0);
SamplerState g_samLinear : register(s0);

cbuffer MeshBuffer:register(b0) {
	matrix world;	// ワールド変換行列
}

cbuffer CameraBuffer : register(b1) {
	matrix view;	// ビュー変換行列
	matrix proj;	// 射影変換行列
}

cbuffer MaterialBuffer : register(b2) {
	float4 diffuse;
	float4 emissive;
	float3 specular;
	float  specular_power;
}

cbuffer BoneBuffer : register(b3) {
	matrix bones[256];
}

struct VS_INPUT {
	float3 Pos			: POSITION;
	float3 Normal		: NORMAL;
	float2 Tex			: TEXCOORD;
	uint4  boneIndices	: BONEINDICES;
	float3 boneWeight	: BONEWEIGHT;
};

struct PS_INPUT {
	float4 Pos		: SV_POSITION;
	float3 PosView	: VIEW_POSITION;
	float3 Normal	: TEXCOORD0;
	float2 Tex		: TEXCOORD1;
};

PS_INPUT VS(VS_INPUT input)
{
	PS_INPUT output;
	float4 pos = float4(input.Pos, 1.0f);
	// ボーンをかける
	uint bi[4] = (uint[4])input.boneIndices;
	float bw[3] = (float[3])input.boneWeight;
	matrix comb = bones[bi[0]] * bw[0] + bones[bi[1]] * bw[1] + bones[bi[2]] * bw[2] + bones[bi[3]] * (1.0f - bw[0] - bw[1] - bw[2]);
	comb = transpose(comb);		// なぜ転置が必要なのかわからん
	pos = mul(pos, comb);
	pos = mul(pos, world);
	output.PosView = mul(pos, view);
	output.Pos = mul(float4(output.PosView, 1.0f), proj);

	// 法線
	float3 normal_head = normalize(input.Normal) + input.Pos.xyz;
	normal_head = mul(float4(normal_head, 1.0f), comb);
	normal_head = mul(float4(normal_head, 1.0f), world);
	output.Normal = normalize(normal_head.xyz - pos.xyz);
	output.Tex = input.Tex;

	return output;
}

float4 PS(PS_INPUT input) : SV_Target
{
	float3 light = float3(0.0f, 1.0f,  1.0f);
	float4 ambient = float4(0.2f, 0.2f, 0.2f, 0.2f);
	light = normalize(light);
	float3 normal = input.Normal;
	// 拡散反射
	float NL = saturate(dot(normal, light));

	// 鏡面反射
	float3 viewDir = normalize(input.PosView.xyz); // posViewはpos(world) - eyeと同じ
	float3 reflect = normalize(2 * NL * normal - light);
	float specularLight = specular_power * pow(saturate(dot(reflect, viewDir)), 2);
	float4 color = diffuse * NL * g_texDecal.Sample(g_samLinear, input.Tex);
	return color + ambient * emissive + float4(specular * specularLight, 1.0);
}

